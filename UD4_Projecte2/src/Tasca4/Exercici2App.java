package Tasca4;

public class Exercici2App {
	
	public static void main(String[] args) {
		/* Declarem tres variables tipus int (N), double (A), char (C). Mostrem per
		 * consola les variables, la suma (N+A), resta (A-N), i el valor num�ric corresponent 
		 * a C de tipo char.
		 */
		int N = 5;
		double A = 4.56, suma = N + A, resta = A - N;
		char C = 'a';
		System.out.println("Variable N = "+ N);
		System.out.println("Variable A = "+ A);	
		System.out.println("Variable C = "+ C);
		System.out.println(N + " + " + A +" = " + suma);
		System.out.println(A + " - " + N +" = " + resta);
		System.out.println("Valor num�ric del car�cter " + C + " = " + (int) C );
	}
}